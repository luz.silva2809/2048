package juego_2048;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridLayout;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.UIManager;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.ActionEvent;


public class Interfaz {

	private JFrame frame;
	private Tablero tablero=new Tablero(); 
	JLabel[][] tablero_visual = new JLabel [4][4]; 
	JLabel score_value = new JLabel(tablero.mostrar_puntaje());


	//Setea los valores del tablero de labels (tablero visual)
	private void actualizarTablero() 
	{
		score_value.setText(tablero.mostrar_puntaje());
		for (int fila = 0; fila < tablero_visual.length; fila++)
		{
			for (int columna = 0; columna < tablero_visual.length; columna++)
			{
				tablero_visual[fila][columna].setText(tablero.mostrar(fila, columna));
			}
		}
	}



	//Muestra un cartel al finalizar el juego con las opciones de volver a jugar y cerrar
	private void perdio() {
		if (tablero.perdio())
		{
			Object[] options= {"Try again", "Close"};
			int n= JOptionPane.showOptionDialog(frame, "Game Over", " ", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
					null, options, options[0]);
			if(n == JOptionPane.YES_OPTION)
			{
				tablero=new Tablero();
				tablero.agregar_nuevo_aleatorio();
				actualizarTablero();
			}
			else if(n == JOptionPane.NO_OPTION)
			{
				frame.dispose();
			}
		}

	}



	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {

				try {

					Interfaz window = new Interfaz();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	// Create the application.
	public Interfaz() {
		juego();
	}



	// Initialize the contents of the frame.
	private void juego() 
	{
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(253, 245, 230));
		frame.setBounds(100, 100, 873, 780);  //873, 780 tama�o original
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().setFocusable(true);


		// Crea el titulo
		JLabel Titulo = new JLabel("2048");
		Titulo.setVerticalAlignment(SwingConstants.TOP);
		Titulo.setHorizontalAlignment(SwingConstants.LEFT);
		Titulo.setForeground(new Color(139, 69, 19));
		Titulo.setFont(new Font("Arial", Font.BOLD, 120));
		Titulo.setBounds(150, 15, 300, 200);
		frame.getContentPane().add(Titulo);		


		// Crea el panel que muestra el puntaje
		JPanel panel_score = new JPanel();
		panel_score.setBackground(Color.LIGHT_GRAY);
		panel_score.setBorder(new LineBorder(UIManager.getColor("Button.shadow")));
		panel_score.setBounds(550, 30, 150, 70);
		frame.getContentPane().add(panel_score);
		panel_score.setLayout(new GridLayout(0, 1, 0, 0));

		JLabel titulo_score = new JLabel("score");
		titulo_score.setForeground(new Color(255, 255, 255));
		titulo_score.setHorizontalAlignment(SwingConstants.CENTER);
		titulo_score.setFont(new Font("Arial", Font.BOLD, 27));
		panel_score.add(titulo_score);

		score_value.setForeground(new Color(255, 255, 255));
		score_value.setBackground(Color.LIGHT_GRAY);
		score_value.setHorizontalAlignment(SwingConstants.CENTER);
		score_value.setFont(new Font("Arial", Font.BOLD, 25));
		panel_score.add(score_value);




		// Boton que reinicia el Juego
		JButton restart = new JButton("New Game");
		restart.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				tablero=new Tablero();
				tablero.agregar_nuevo_aleatorio();
				actualizarTablero();
				restart.setFocusable(false);
			}
		});
		restart.setForeground(new Color(255, 255, 255));
		restart.setBackground(new Color(184, 134, 11));
		restart.setFont(new Font("Arial", Font.BOLD, 18));
		restart.setBounds(550, 120, 145, 25);
		frame.getContentPane().add(restart);



		// Panel que muestra el tablero	
		JPanel panel_tablero = new JPanel();
		panel_tablero.setBorder(new LineBorder(UIManager.getColor("Button.shadow")));
		panel_tablero.setBackground(Color.LIGHT_GRAY);
		panel_tablero.setBounds(150, 170, 550, 550);
		frame.getContentPane().add(panel_tablero);
		panel_tablero.setLayout(new GridLayout(0, 4, 0, 0));



		// Posiciona las celdas visuales en el tablero
		for (int fila = 0; fila < tablero_visual.length; fila++)
		{
			for (int columna = 0; columna < tablero_visual.length; columna++)
			{
				tablero_visual[fila][columna] = new JLabel(tablero.mostrar(fila,columna));
				tablero_visual[fila][columna].setForeground(Color.WHITE);
				tablero_visual[fila][columna].setHorizontalAlignment(SwingConstants.CENTER);
				tablero_visual[fila][columna].setBackground(new Color(210, 180, 140));
				tablero_visual[fila][columna].setFont(new Font("Arial", Font.BOLD, 38));
				tablero_visual[fila][columna].setBorder(new LineBorder(new Color(169, 169, 169), 12));
				panel_tablero.add(tablero_visual[fila][columna]);
			}
		}



		//JUEGO
		tablero.agregar_nuevo_aleatorio();
		actualizarTablero();


		frame.getContentPane().addKeyListener(new KeyListener()
		{
			@Override
			public void keyTyped(KeyEvent e) {

			}

			@Override

			public void keyPressed(KeyEvent evt) 
			{
				if(!tablero.perdio()) {
					if(evt.getKeyCode() == KeyEvent.VK_UP)
					{
						tablero.mover_arriba();
						tablero.agregar_nuevo_aleatorio();
						actualizarTablero();
						perdio();
					}
					if(evt.getKeyCode() == KeyEvent.VK_DOWN)
					{
						tablero.mover_abajo();
						tablero.agregar_nuevo_aleatorio();
						actualizarTablero();
						perdio();
					}
					if(evt.getKeyCode() == KeyEvent.VK_LEFT)
					{
						tablero.mover_izquierda();
						tablero.agregar_nuevo_aleatorio();
						actualizarTablero();
						perdio();
					}
					if(evt.getKeyCode() == KeyEvent.VK_RIGHT)
					{
						tablero.mover_derecha();
						tablero.agregar_nuevo_aleatorio();
						actualizarTablero();
						perdio();
					}

					
				}

			}
			@Override
			public void keyReleased(KeyEvent evt) {

			}
		});

	}

}
	

