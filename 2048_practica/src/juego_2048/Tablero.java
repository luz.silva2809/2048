package juego_2048;

import java.util.Random;

public class Tablero
{
	private int [][] tablero;
	private int puntaje_juego;
	
	//Constructor de tablero
	public Tablero() 
	{
		this.tablero = new int[4][4];
		this.puntaje_juego=0;
	}

	
	//Desplaza los valores del tablero a izquierda y suma los iguales consecutivos
	public void mover_izquierda() 						 
	{													
		for(int fila=0;fila<tamanio();fila++)
		{
			int [] arregloAlineado=arreglo_alineado("izquierda","fila",fila);

			for(int indice=0; indice< tamanio();indice++)
			{
				if(indice != tamanio() -1)
				{
					if((arregloAlineado[indice] != 0) && arregloAlineado[indice] == arregloAlineado[indice+1])
					{
						arregloAlineado[indice] = arregloAlineado[indice]*2;
						arregloAlineado[indice+1] = 0;
						score(arregloAlineado[indice]);
					}
				}
				
				if(indice>0 && arregloAlineado[indice-1] == 0 && arregloAlineado[indice]!=0) 
				{
					this.tablero[fila][indice-1]=arregloAlineado[indice];
					this.tablero[fila][indice]=0;
					arregloAlineado[indice-1]=arregloAlineado[indice];
					arregloAlineado[indice]=0;
				}
				else
				{
					this.tablero[fila][indice]=arregloAlineado[indice];
				}
			}
		}
	}


	//Desplaza los valores del tablero hacia derecha y suma los igulaes consecutivos
	public void mover_derecha() 						
	{																		
		for(int fila=0;fila<tamanio();fila++)
		{
			int [] alineados_a_derecha=arreglo_alineado("derecha","fila",fila);

			for(int indice=tamanio()-1; indice>=0 ;indice--)
			{
				
				if(indice != 0)
				{
					if((alineados_a_derecha[indice] != 0) && alineados_a_derecha[indice] == alineados_a_derecha[indice-1])
					{
						alineados_a_derecha[indice] = alineados_a_derecha[indice]*2;
						alineados_a_derecha[indice-1] = 0;
						score(alineados_a_derecha[indice]);
					}
				}
				
				if(indice<tamanio()-1 && alineados_a_derecha[indice+1] == 0 && alineados_a_derecha[indice]!=0) 
				{
					this.tablero[fila][indice+1]=alineados_a_derecha[indice];
					this.tablero[fila][indice]=0;
					alineados_a_derecha[indice+1]=alineados_a_derecha[indice];
					alineados_a_derecha[indice]=0;
				}
				else
					{
					this.tablero[fila][indice]=alineados_a_derecha[indice];
					}
			}
		}	
	}
	
	
	//Desplaza los valores del tablero hacia abajo y suma los iguales consecutivos
	public void mover_abajo() 						
	{																		
		for(int columna=0;columna<this.tablero.length;columna++)
		{
			int [] alineados_a_derecha=arreglo_alineado("derecha", "columna",columna);
						
			for(int indice=tamanio()-1; indice>=0 ;indice--)
			{
				if(indice != 0)
				{
					if((alineados_a_derecha[indice] != 0) && alineados_a_derecha[indice] == alineados_a_derecha[indice-1])
					{
						alineados_a_derecha[indice] = alineados_a_derecha[indice]*2;
						alineados_a_derecha[indice-1] = 0;
						score(alineados_a_derecha[indice]);
					}
				}
				
				if(indice<tamanio()-1 && alineados_a_derecha[indice+1] == 0 && alineados_a_derecha[indice]!=0) 
				{
					this.tablero[indice+1][columna]=alineados_a_derecha[indice];
					this.tablero[indice][columna]=0;
					alineados_a_derecha[indice+1]=alineados_a_derecha[indice];
					alineados_a_derecha[indice]=0;
				}
				else
					{
					this.tablero[indice][columna]=alineados_a_derecha[indice];
					}
			}
		}	
		}
	
	
	//Desplaza los valores del tablero hacia arriba y suma los iguales consecutivos
	public void mover_arriba() 						
	{
		for(int columna=0;columna<this.tablero.length;columna++)
		{
			int [] alineados_a_izquierda=arreglo_alineado("izquierda", "columna", columna);

			for(int indice=0; indice< tamanio();indice++)
			{
				if(indice != tamanio()-1)
				{
					if((alineados_a_izquierda[indice] != 0) && alineados_a_izquierda[indice] == alineados_a_izquierda[indice+1])
					{
						alineados_a_izquierda[indice] = alineados_a_izquierda[indice]*2;
						alineados_a_izquierda[indice+1] = 0;
						score(alineados_a_izquierda[indice]);
					}
				}
				
				if(indice>0 && alineados_a_izquierda[indice-1] == 0 && alineados_a_izquierda[indice]!=0) 
				{
					this.tablero[indice-1][columna]=alineados_a_izquierda[indice];
					this.tablero[indice][columna]=0;
					alineados_a_izquierda[indice-1]=alineados_a_izquierda[indice];
					alineados_a_izquierda[indice]=0;
				}
				else
				{
					this.tablero[indice][columna]=alineados_a_izquierda[indice];
				}
			}
			
		}
	}

	
	
	//Agrega nuevo valor en posicion aleatoria	
	public void agregar_nuevo_aleatorio()
	{
		int dos_o_cuatro=numero_aleatorio();
		int fila = posicion_aleatoria();
		int columna = posicion_aleatoria();
		if(!tablero_lleno())
		{
			while(posicionOcupada(fila, columna))
			{
				fila = posicion_aleatoria();
				columna = posicion_aleatoria();
			}
			this.tablero[fila][columna]=dos_o_cuatro;
		}
	}
	
	
	
	//Devuelve true cuando no hay posibilidad de movimiento (el tablero lleno y sin adyacentes iguales)
		public boolean perdio() 
		{
			return (!se_puede_mover())? true: false ;
		}
	

	//Muestra el valor que contiene una posicion determinada en el tablero como texto
	public String mostrar(int fila, int columna)  		
	{
		Integer valor=this.tablero[fila][columna];
		
		if(this.tablero[fila][columna] == 0)
		{
			return "";
		}
		return valor.toString();
	}
	
	
	//Mostrar puntaje actual
	public String mostrar_puntaje()
	{
		Integer puntaje_actual=this.puntaje_juego;
		return puntaje_actual.toString();
	}
	
	
	//Devuelve true si el tablero tiene todas sus casillas ocupadas
	private boolean tablero_lleno()
	{
		for (int fila = 0; fila < tamanio() ; fila++)
		{
			for (int columna = 0; columna < tamanio() ; columna++)
			{
				if(!posicionOcupada(fila,columna))
					return false;
			}	
		}
		return true;
	}
	

	//Devuelve true cuando un numero es igual a su adyacente
	private boolean es_igual_al_adyacente(int fila, int columna, String direccion_a_comparar)
	{
		if(direccion_a_comparar.equals("derecha"))
			return this.tablero[fila][columna] == this.tablero[fila][columna+1];
		
		if(direccion_a_comparar.equals("abajo"))
			return this.tablero[fila][columna] == this.tablero[fila+1][columna];
		
		else
			return false;
	}

	
	//Devuelve true cuando en el tablero hay al menos dos valores adyacentes iguales o una posicion desocupada
	private boolean se_puede_mover()
		{
			if(tablero_lleno())
			{
				for(int fila=0;fila<tamanio();fila++) 
				{
					for(int columna=0;columna <tamanio(); columna++)
					{
						String comparar_hacia_abajo="abajo";
						String comparar_hacia_derecha="derecha";
		
						
						if(fila == 3 && columna <3)
						{
							if(es_igual_al_adyacente(fila,columna, comparar_hacia_derecha))
							{
								return true;
							}
								
						}
						if(columna == 3 && fila<3)
						{
							if(es_igual_al_adyacente(fila, columna, comparar_hacia_abajo))
							{
								return true;
							}
						}
						if(fila !=3 && columna !=3)
						{
							if (es_igual_al_adyacente(fila,columna, comparar_hacia_derecha) 
								|| es_igual_al_adyacente(fila, columna, comparar_hacia_abajo))
							{
								return true;
							}
						}	
					}
				}
				
				return false;
			}
			
			else 
			{
				return true;
			}
			
		}

	
	
	//Crea un arreglo de una fila o columna con sus valores (distintos de cero) alineados a derecha o izquierda	
	private int [] arreglo_alineado(String direccion_a_alinear, String es_fila_o_columna, int indice)
	{
		int [] resultado=new int [4];
		int indice_en_resultado=0;
		
		if(direccion_a_alinear.equals("derecha"))
		{
			//Genera arreglo alineado a derecha
			indice_en_resultado=resultado.length-1;
			
			if(es_fila_o_columna.equals("fila"))
			{
				for(int columna=tamanio()-1; columna>=0; columna--)
				{
					if(this.tablero[indice][columna] != 0)
					{
						resultado[indice_en_resultado]=this.tablero[indice][columna];
						indice_en_resultado--;
					}
				}
				return resultado;
			}
			else
			{
				for(int fila=tamanio()-1; fila>=0; fila--)
				{
					if(this.tablero[fila][indice] != 0)
					{
						resultado[indice_en_resultado]=this.tablero[fila][indice];
						indice_en_resultado--;
					}
				}
				return resultado;
			}
		}
		else
		{
			//Genera arreglo alineado a izquierda
			if(es_fila_o_columna.equals("fila"))
			{
				for(int columna=0; columna<tamanio(); columna++)
				{
					if(this.tablero[indice][columna] != 0)
					{
						resultado[indice_en_resultado]=this.tablero[indice][columna];
						indice_en_resultado++;
					}
				}
				return resultado;
			}
			else
			{
				for(int fila=0; fila<tamanio(); fila++)
				{
					if(this.tablero[fila][indice] != 0)
					{
						resultado[indice_en_resultado]=this.tablero[fila][indice];
						indice_en_resultado++;
					}
				}
				return resultado;
			}
		}
	}
	
	
	
	//Devuelve true cuando la posicion pasada como parametro esta ocupada, o sea, tiene un valor distinto de cero
	private boolean posicionOcupada(int fila, int columna) 
	{
		return ( this.tablero[fila][columna] != 0);
	}
	
	
	
	//Devuelve el tama�o de una fila del arreglo
	private int tamanio() 
	{
		return this.tablero.length;
	}

	
	//Generador de numero aleatorio dos o cuatro
	private int numero_aleatorio()
	{
		int dos=2, cuatro=4;
		return new Random().nextBoolean() ? dos : cuatro;
	}
	
	
	//Generador de posicion aleatoria
	private int posicion_aleatoria()
	{
		return new Random().nextInt(4);
	}
	
	
	//Modifica los valores del tablero de puntaje
	private void score(int puntaje)
	{
		this.puntaje_juego+=puntaje;
	}
	
}
